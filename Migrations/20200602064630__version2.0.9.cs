﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EC_API.Migrations
{
    public partial class _version209 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ArtNo",
                table: "ModelNames",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArtNo",
                table: "ModelNames");
        }
    }
}
