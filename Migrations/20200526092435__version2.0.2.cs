﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EC_API.Migrations
{
    public partial class _version202 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "MaterialName",
                table: "Glues",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModelNo",
                table: "Glues",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PathName",
                table: "Glues",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaterialName",
                table: "Glues");

            migrationBuilder.DropColumn(
                name: "ModelNo",
                table: "Glues");

            migrationBuilder.DropColumn(
                name: "PathName",
                table: "Glues");
        }
    }
}
