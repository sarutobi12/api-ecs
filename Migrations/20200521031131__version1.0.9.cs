﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EC_API.Migrations
{
    public partial class _version109 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropColumn(
            //    name: "ModelNoID",
            //    table: "ModelNames");

            //migrationBuilder.AddColumn<string>(
            //    name: "ModelNo",
            //    table: "ModelNames",
            //    nullable: true);

            //migrationBuilder.CreateTable(
            //    name: "MaterialName",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_MaterialName", x => x.ID);
            //    });

            //migrationBuilder.CreateTable(
            //    name: "PartName",
            //    columns: table => new
            //    {
            //        ID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:Identity", "1, 1"),
            //        Name = table.Column<string>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_PartName", x => x.ID);
            //    });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MaterialName");

            migrationBuilder.DropTable(
                name: "PartName");

            migrationBuilder.DropColumn(
                name: "ModelNo",
                table: "ModelNames");

            //migrationBuilder.AddColumn<int>(
            //    name: "ModelNoID",
            //    table: "ModelNames",
            //    type: "int",
            //    nullable: false,
            //    defaultValue: 0);
        }
    }
}
