﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EC_API.Migrations
{
    public partial class add_consumption : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Consumption",
                table: "Glues",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Consumption",
                table: "Glues");
        }
    }
}
