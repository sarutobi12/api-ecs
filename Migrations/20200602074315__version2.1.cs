﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EC_API.Migrations
{
    public partial class _version21 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GlueID",
                table: "Plans");

            migrationBuilder.DropColumn(
                name: "LineID",
                table: "Plans");

            migrationBuilder.AddColumn<string>(
                name: "GlueName",
                table: "Plans",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LineName",
                table: "Plans",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GlueName",
                table: "Plans");

            migrationBuilder.DropColumn(
                name: "LineName",
                table: "Plans");

            migrationBuilder.AddColumn<int>(
                name: "GlueID",
                table: "Plans",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LineID",
                table: "Plans",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
