using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EC_API.Helpers;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EC_API._Repositories.Interface;
using EC_API._Services.Interface;
using EC_API.DTO;
using EC_API.Models;
using Microsoft.EntityFrameworkCore;

namespace EC_API._Services.Services
{
    public class GlueService : IGlueService
    {
        private readonly IGlueRepository _repoGlue;
        private readonly IPartNameRepository _repoPartName;
        private readonly IPartName2Repository _repoPartName2;
        private readonly IMaterialNameRepository _repoMaterialName;
        private readonly IModelNameRepository _repoModelName;
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _configMapper;
        public GlueService(IGlueRepository repoBrand,IModelNameRepository repoModelName, IPartNameRepository repoPartName, IPartName2Repository repoPartName2, IMaterialNameRepository repoMaterialName, IMapper mapper, MapperConfiguration configMapper)
        {
            _configMapper = configMapper;
            _mapper = mapper;
            _repoGlue = repoBrand;
            _repoPartName = repoPartName;
            _repoPartName2 = repoPartName2;
            _repoMaterialName = repoMaterialName;
            _repoModelName = repoModelName;

        }

        //Thêm Brand mới vào bảng Glue
        public async Task<bool> Add(GlueCreateDto model)
        {
            var glue = _mapper.Map<Glue>(model);
            _repoGlue.Add(glue);
            return await _repoGlue.SaveAll();
        }


        public async Task<bool> Add1(GlueCreateDto1 model)
        {
            if(model.ModelNoID == 0)
            {
                model.ModelNo = "";
            }
            else
            {

                model.ModelNo = _repoPartName.GetAll().FirstOrDefault(a => a.ID == model.ModelNoID).Name;
            }

            if(model.PartNameID == 0)
            {
                model.PathName = "";
            }
            else
            {
                model.PathName = _repoPartName2.GetAll().FirstOrDefault(a => a.ID == model.PartNameID).Name;
            }

            if (model.MaterialNameID == 0)
            {
                model.MaterialName = "";
            }
            else
            {
                model.MaterialName = _repoMaterialName.GetAll().FirstOrDefault(a => a.ID == model.MaterialNameID).Name;
            }

            model.Name = model.GlueName;
            var glue = _mapper.Map<Glue>(model);
            _repoGlue.Add(glue);


            return await _repoGlue.SaveAll();
        }


        //Lấy danh sách Brand và phân trang
        public async Task<PagedList<GlueCreateDto>> GetWithPaginations(PaginationParams param)
        {
            var lists = _repoGlue.FindAll().ProjectTo<GlueCreateDto>(_configMapper).OrderByDescending(x => x.ID);
            return await PagedList<GlueCreateDto>.CreateAsync(lists, param.PageNumber, param.PageSize);
        }

        //public async Task<object> GetIngredientOfGlue(int glueid)
        //{
        //    return await _repoGlue.GetIngredientOfGlue(glueid);

        //    throw new System.NotImplementedException();
        //}

        //Tìm kiếm glue
        public async Task<PagedList<GlueCreateDto>> Search(PaginationParams param, object text)
        {
            var lists = _repoGlue.FindAll().ProjectTo<GlueCreateDto>(_configMapper)
            .Where(x => x.Code.Contains(text.ToString()))
            .OrderByDescending(x => x.ID);
            return await PagedList<GlueCreateDto>.CreateAsync(lists, param.PageNumber, param.PageSize);
        }

        public async Task<bool> CheckExists(int id)
        {
            return await _repoGlue.CheckExists(id);
        }

        //Xóa Brand
        public async Task<bool> Delete(object id)
        {
            var glue = _repoGlue.FindById(id);
            _repoGlue.Remove(glue);
            return await _repoGlue.SaveAll();
        }

        //Cập nhật Brand
        public async Task<bool> Update(GlueCreateDto model)
        {
            var glue = _mapper.Map<Glue>(model);
            _repoGlue.Update(glue);
            return await _repoGlue.SaveAll();
        }

        //Lấy toàn bộ danh sách Brand 
        public async Task<List<GlueCreateDto>> GetAllAsync()
        {
            return await _repoGlue.FindAll().ProjectTo<GlueCreateDto>(_configMapper).OrderByDescending(x => x.ID).ToListAsync();
        }

        public async Task<List<GlueCreateDto1>> GetAllAsyncByModalName(int modelNameID)
        {
            var modelname = _repoModelName.GetAll();
            var partname = _repoPartName.GetAll();
            var materialname = _repoMaterialName.GetAll();
            var lists = await _repoGlue.FindAll().ProjectTo<GlueCreateDto1>(_configMapper).Where(x => x.ModalNameID == modelNameID).OrderByDescending(x => x.ID).Select(x => new GlueCreateDto1
            {
                ID = x.ID,
                Name = x.Name,
                Code = x.Code,
                ModelNo = x.ModelNo,
                CreatedDate = x.CreatedDate,
                ModalNameID = x.ModalNameID,
                PathName = x.PathName,
                PartNameID = x.PartNameID,
                MaterialNameID = x.MaterialNameID,
                MaterialName = x.MaterialName,
                Consumption = x.Consumption,
            }).ToListAsync();
            return lists;

            //return await _repoGlue.FindAll().ProjectTo<GlueCreateDto1>(_configMapper).Where(x=>x.ModalNameID == modelNameID).OrderByDescending(x => x.ID).ToListAsync();
        }


        //Lấy Brand theo Brand_Id
        public GlueCreateDto GetById(object id)
        {
            return  _mapper.Map<Glue, GlueCreateDto>(_repoGlue.FindById(id));
        }

        public async Task<bool> CheckBarCodeExists(string code)
        {
            return await _repoGlue.CheckBarCodeExists(code);

        }
    }
}