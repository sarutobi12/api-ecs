﻿using EC_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EC_API.DTO
{
    public class GlueCreateDto1
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string CreatedDate { get; set; }
        public string GlueName { get; set; }
        public string ModelNo { get; set; }
        public int ModelNoID { get; set; }
        public int ModalNameID { get; set; }
        public string PathName { get; set; }
        public int PartNameID { get; set; }
        public string MaterialName { get; set; }
        public string Consumption { get; set; }
        public int MaterialNameID { get; set; }
        public List<Ingredient> Ingredients { get; set; }
    }
}
