using EC_API.DTO;
using EC_API.Models;
using AutoMapper;

namespace EC_API.Helpers.AutoMapper
{
    public class EfToDtoMappingProfile : Profile
    {
        char[] alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        public EfToDtoMappingProfile()
        {
            CreateMap<User, UserForDetailDto>();
            CreateMap<Glue, GlueDto>().ForMember(d => d.CreatedDate, o => o.MapFrom(s => s.CreatedDate.ToParseStringDateTime()));
            CreateMap<Glue, GlueCreateDto>().ForMember(d => d.CreatedDate, o => o.MapFrom(s => s.CreatedDate.ToParseStringDateTime()));
            CreateMap<Glue, GlueCreateDto1>().ForMember(d => d.CreatedDate, o => o.MapFrom(s => s.CreatedDate.ToParseStringDateTime()));
            CreateMap<Ingredient, IngredientDto>().ForMember(d => d.CreatedDate, o => o.MapFrom(s => s.CreatedDate.ToParseStringDateTime()));
            CreateMap<Ingredient, IngredientDto1>().ForMember(d => d.CreatedDate, o => o.MapFrom(s => s.CreatedDate.ToParseStringDateTime()));/*.ForMember(d => d.Position, o => o.MapFrom(s => s.Position != null ? alpha[s.Position -1] : ""))*/;

            CreateMap<Line, LineDto>();
            CreateMap<ModelNo, ModelNoDto>().ForMember(d => d.ModelName, o => o.MapFrom(s => s.ModelName.Name));
            CreateMap<ModelNo, ModelNoForMapModelDto>();

            CreateMap<ModelName, ModelNameDto>().ForMember(d => d.ModelNumberDtos, o => o.MapFrom(s => s.ModelNos));
            CreateMap<Plan, PlanDto>();

            CreateMap<MapModel, MapModelDto>();
            CreateMap<UserDetailDto, UserDetail> ();
            CreateMap<Supplier, SuppilerDto>();
            CreateMap<PartName, PartNameDto>();
            CreateMap<PartName2, PartName2Dto>();
            CreateMap<MaterialName, MaterialNameDto>();

        }

    }
}